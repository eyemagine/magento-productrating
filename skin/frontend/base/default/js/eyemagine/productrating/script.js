(function($) {

    $(document).ready(function() {

        if ($("#product-review-table").length > 0) {

            var that = $("#product-review-table"),
            headers = that.find("thead tr:first-child"),
            body = that.find("tbody"),
            starHtml = new Array(
                headers.children().eq(1).contents(),
                headers.children().eq(2).contents(),
                headers.children().eq(3).contents(),
                headers.children().eq(4).contents(),
                headers.children().eq(5).contents()
            );

            headers.parent().remove();

            body.find("tr").each(function() {

                row = $(this);

                row.find(".value").hide();

                var rating = $("<td></td>", {
                    html: starHtml[4].clone()
                }).appendTo(row)
                .children()
                .css("cursor", "pointer"),
                width = rating.width(),
                setWidth = 0;

                rating.find(".rating").width(setWidth);

                rating.mousemove(function(e) {

                    rating.find(".rating").width(Math.ceil(((e.pageX - rating.offset().left) / width) * 5) * 20);
                });
                
                rating.mouseout(function() {

                    rating.find(".rating").width(setWidth);
                });
                
                rating.click(function(e) {

                    var selected = Math.ceil(((e.pageX - $(this).offset().left) / width) * 5);

                    setWidth = selected * 20;

                    $(this).find(".rating").width(setWidth);

                    $(this).closest("tr").find("[name^=ratings]").prop("checked", false).eq(selected - 1).prop("checked", true);
                });
            });
        }
    });
})(jQuery);