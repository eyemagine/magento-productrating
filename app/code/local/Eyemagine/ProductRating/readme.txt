/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * ProductRating
 *
 * @author EYEMAGINE <magento@eyemaginetech.com>
 * @category Eyemagine
 * @package Eyemagine_ProductRating
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

-------------------------------------------------------------------------------
DESCRIPTION:
-------------------------------------------------------------------------------

This is an overview of the module's features. This can be the same text that is
used to describe the module as it would appear on Magento Connect or within the
requirement document for a custom client module.  This file is NOT intended to
be the all-inclusive documentation for the module, although that might be the
case for very simple modules.

Additional Features:

  - Feature one
  - Another really cool feature
  - Can do this thing if you really want
  - NOTE: will not do this

Module Files:

  - app/etc/modules/Eyemagine_ProductRating.xml
  - app/code/local/Eyemagine/ProductRating/*
  - app/design/frontend/base/default/layout/eyemagine/productrating.xml
  - skin/frontend/base/default/js/eyemagine/productrating.js


-------------------------------------------------------------------------------
COMPATIBILITY:
-------------------------------------------------------------------------------

  - Magento Enterprise Edition 1.12.0
  

-------------------------------------------------------------------------------
RELEASE NOTES:
-------------------------------------------------------------------------------
    
v0.1.0: March 8, 2013
  - Initial release